package kodune;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class Lips {
	
	double xpoints[];
	double ypoints[];
	double xpoints1[];
	double ypoints1[];
	
	public Lips (double[] p1, double[] p2, double[] p3, double[] p4){
		xpoints = p1;
		ypoints = p2;
		xpoints1 = p3;
		ypoints1 = p4;
	}	
	
	public void joonistaLips(GraphicsContext gc) {
		// lips vasak pool
		gc.setFill(Color.RED);
		
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);
		// lips parem pool
		gc.setFill(Color.RED);
		
		int num1 = 3;
		gc.fillPolygon(xpoints1, ypoints1, num1);
		// lipsu kesk
		gc.setFill(Color.RED);
		gc.fillOval(190, 255, 20, 30);

	}

	public void setCursor(Cursor hand) {
		// TODO Auto-generated method stub
		
	}

	public void setOnMousePressed(EventHandler<MouseEvent> circleOnMousePressedEventHandler) {
		// TODO Auto-generated method stub
		
	}

	public double getTranslateX() {
		// TODO Auto-generated method stub
		return 0;
	}

	public double getTranslateY() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setTranslateX(double newTranslateX) {
		// TODO Auto-generated method stub
		
	}

	public void setTranslateY(double newTranslateY) {
		// TODO Auto-generated method stub
		
	}

	public void setOnMouseDragged(EventHandler<MouseEvent> circleOnMouseDraggedEventHandler) {
		// TODO Auto-generated method stub
		
	}


}
