package kodune;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class EyeShadow {
	public void joonistaEyeShadow(GraphicsContext gc) {

		// silma sisemused valged
		gc.setFill(Color.WHITE);
		gc.fillOval(150, 90, 50, 70);
		gc.fillOval(200, 90, 50, 70);

	}
}
