package kodune;

import java.awt.*;
import java.util.Random;

import com.sun.org.apache.bcel.internal.generic.ALOAD;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import javafx.stage.Stage;
import riided.TopHat;
import javafx.scene.Cursor;

public class GraafikaNaide extends Application {
	Lips lips;
	double orgSceneX, orgSceneY;
	double orgTranslateX, orgTranslateY;

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistatud kloun");
		Group root = new Group();
		Canvas canvas = new Canvas(1000, 1000);
		GraphicsContext gc = canvas.getGraphicsContext2D();

		// Tegelase joonistaminee
		joonistaKael(gc);
		joonistaKeha(gc);
		joonistaNagu(gc);

		joonista(gc);
		joonistaSilmad(gc);
		joonistaHuuled(gc);
		joonistaSuuSisemus(gc);
		joonistaTekst(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {

		double xpoints[] = { 242, 242, 200 };
		double ypoints[] = { 245, 290, 270 };

		double xpoints1[] = { 160, 160, 200 };
		double ypoints1[] = { 245, 290, 270 };
		Lips lisp = new Lips(xpoints, ypoints, xpoints1, ypoints1);
		lisp.joonistaLips(gc);
		lisp.setCursor(Cursor.HAND);
		lisp.setOnMousePressed(circleOnMousePressedEventHandler);
		lisp.setOnMouseDragged(circleOnMouseDraggedEventHandler);

		JuuksedV juuksedV = new JuuksedV();
		juuksedV.joonistaJuuksedV(gc);

		JuuksedP juuksedP = new JuuksedP();
		juuksedP.joonistaJuuksedP(gc);

		MytsKolmnurk mytsKolmnurk = new MytsKolmnurk();
		mytsKolmnurk.joonistMytsKolmnurk(gc);

		TopHat topHat = new TopHat();
		topHat.joonistaTopHat(gc);

		Blush blush = new Blush();
		blush.joonistaBlush(gc);

		EyeShadow eyeShadow = new EyeShadow();
		eyeShadow.joonistaEyeShadow(gc);

		SuuTaust suuTaust = new SuuTaust();
		suuTaust.joonistaSuuTaust(gc);

		Nina nina = new Nina();
		nina.joonistaNina(gc);

	}

	private void joonistaKael(GraphicsContext gc) {
		gc.setFill(Color.WHEAT);
		gc.fillRoundRect(175, 240, 50, 40, 20, 20);
	}

	private void joonistaKeha(GraphicsContext gc) {
		// Keha
		gc.setFill(Color.PURPLE);
		gc.fillRoundRect(75, 260, 250, 250, 20, 20);
	}

	private void joonistaNagu(GraphicsContext gc) {
		// näo põhi
		gc.setFill(Color.WHEAT);
		gc.fillOval(100, 50, 200, 200);

	}

	private void joonistaSilmad(GraphicsContext gc) {

		// silma valged
		gc.setFill(Color.WHITE);
		gc.fillOval(158, 121, 35, 10);
		gc.fillOval(208, 121, 35, 10);

		// Silma puhkpillid
		gc.setFill(Color.BLACK);
		gc.fillOval(170, 120, 10, 10);
		gc.fillOval(220, 120, 10, 10);

		// silma kontuur sinine
		gc.setStroke(Color.BLUE);
		gc.setLineWidth(1);
		// silma puhkpilli stroke
		gc.strokeOval(158, 121, 35, 10);
		gc.strokeOval(208, 121, 35, 10);

	}

	private void joonistaHuuled(GraphicsContext gc) {
		// Huuled
		gc.setFill(Color.RED);
		gc.fillRoundRect(155, 183, 90, 40, 100, 100);
	}

	private void joonistaSuuSisemus(GraphicsContext gc) {

		// suu sisemus
		gc.setFill(Color.DARKRED);
		gc.fillRoundRect(170, 190, 60, 25, 100, 100);
		// hambad
		gc.setFill(Color.WHITE);
		// ülemised
		gc.fillRoundRect(185, 190, 30, 5, 50, 50);
		// alumised
		gc.fillRoundRect(185, 210, 30, 5, 50, 50);
	}

	private void joonistaTekst(GraphicsContext gc) {
		// gc.setFill(Color.GREEN);
		int R = (int) (Math.random() * 256);
		int G = (int) (Math.random() * 256);
		int B = (int) (Math.random() * 256);
		// Color color = new Color(R, G, B);
		// nii saab teha ise värve
		Color minuV2rv = Color.rgb(R, G, B);
		// gc.setLineWidth(5);
		// gc.fillRoundRect(x, y, w, h, arcWidth, arcHeight);
		// gc.fillRoundRect(50, 50, 300, 300, 40, 40);
		// gc.strokeLine(x1, y1, x2, y2);
		// gc.strokeLine(100, 300, 300, 300);
		// gc.strokeOval(x, y, w, h);

		// silma make-up ääre stroke
		// gc.strokeOval(150, 100, 50, 50);
		// gc.strokeOval(200, 100, 50, 50);

		gc.setFill(minuV2rv);
		gc.fillText("Missed me, kids?MWAHAHAHAHAHAH", 100, 370);

	}

	public static void main(String[] args) {

		launch(args);

	}

	EventHandler<MouseEvent> circleOnMousePressedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent t) {
			orgSceneX = t.getSceneX();
			orgSceneY = t.getSceneY();
			orgTranslateX = ((Lips) (t.getSource())).getTranslateX();
			orgTranslateY = ((Lips) (t.getSource())).getTranslateY();
		}
	};

	EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent t) {
			double offsetX = t.getSceneX() - orgSceneX;
			double offsetY = t.getSceneY() - orgSceneY;
			double newTranslateX = orgTranslateX + offsetX;
			double newTranslateY = orgTranslateY + offsetY;

			((Lips) (t.getSource())).setTranslateX(newTranslateX);
			((Lips) (t.getSource())).setTranslateY(newTranslateY);
		}
	};

}