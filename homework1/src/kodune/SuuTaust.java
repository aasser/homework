package kodune;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class SuuTaust {
	public void joonistaSuuTaust(GraphicsContext gc) {
		// See on suu valge
		gc.setStroke(Color.WHITE);
		gc.setLineWidth(30);
		gc.strokeRoundRect(155, 183, 90, 40, 60, 60);
	}
}
