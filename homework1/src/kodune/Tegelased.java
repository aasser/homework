package kodune;

public class Tegelased {

	 static public void main (String[] argumendid) {
		 
		System.out.println("Mida tegelased ytlevad?");
		Tegelane minuTegelane = new Tegelane("Patu");
		System.out.println(minuTegelane); // kasutab toString() meetodit
		Kloun minuKloun = new Kloun("Joker");
		System.out.println(minuKloun);
		minuTegelane = minuKloun;
		System.out.println(minuTegelane);
		V6lur minuV6lur = new V6lur("V6lur Amadeus");
		System.out.println(minuV6lur);
		 
	 }
	
}
