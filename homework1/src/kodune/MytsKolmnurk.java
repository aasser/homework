package kodune;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MytsKolmnurk {
	
	
	public void joonistMytsKolmnurk(GraphicsContext gc) {

		// Müts kolmnurk
		gc.setFill(Color.BLACK);
		double xpoints[] = { 80, 200, 320 };
		double ypoints[] = { 80, 0, 80 };
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);

	}

}
