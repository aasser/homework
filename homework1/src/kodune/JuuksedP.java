package kodune;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class JuuksedP {

	public void joonistaJuuksedP(GraphicsContext gc) {
		// Juuksed parem pool
		gc.setFill(Color.ORANGE);
		gc.fillOval(250, 55, 50, 50);
		gc.fillOval(275, 80, 30, 30);
		gc.fillOval(250, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(250, 55, 50, 50);

	}
	
	
}
