package kodune;

public class Tegelane {

	private String nimi;

	Tegelane(String s) { // konstruktor
		paneNimi(s);
	}

	public String votaNimi() {
		return nimi;
	}

	public void paneNimi(String s) {
		nimi = s;
	}

	public String toString() { // katame yle
		return "Olen tegelane " + votaNimi();
	}

}
